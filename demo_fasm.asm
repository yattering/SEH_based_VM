; SEH based VM Engine by Yattering, 2016
; e-mail: yattering (at) sigaint (d0t) org
; jabber: yattering (at) xmpp (d0t) jp

format MS COFF

include 'sehvm.inc'

extrn '__imp__MessageBoxA@16' as MessageBoxA:dword

extrn '_exception_handler' as _exception_handler

public _main

section '.text' code readable executable
_main:
        ; Register exception handler
        lea     eax, [_exception_handler]
        xor     ecx, ecx
        push    eax
        push    DWord [fs:ecx]
        mov     DWord [fs:ecx], esp
        ; Run unprotected code
        call _unprotected_code_function
        ; Run protecred code
        SEHVM_CALL_REL_C32 _protected_code_function
        ; Unregister exception handler
        xor     ecx, ecx
        pop     DWord [fs:ecx]
        pop     eax
        ret

_protected_code_function:
	SEHVM_PUSH_C32 0
        SEHVM_PUSH_C32 _caption
        SEHVM_PUSH_C32 _messageP
        SEHVM_PUSH_C32 0
        call DWord [MessageBoxA]
        SEHVM_RET_C8 0

_unprotected_code_function:
	push 0
        push _caption
        push _messageUP
        push 0
        call DWord [MessageBoxA]
	ret

section '.data' data readable writeable

 _caption   db 'Win32 assembly',0
 _messageUP db 'MessageBox from unprotected code',0
 _messageP  db 'MessageBox from protected code',0