; SEH based VM Engine by Yattering, 2016
; e-mail: yattering (at) sigaint (d0t) org
; jabber: yattering (at) xmpp (d0t) jp

SEHVM_TYPE_RET_C8 equ 0x0
SEHVM_TYPE_PUSH_C32 equ 0x1
SEHVM_TYPE_PUSH_R32 equ 0x2
SEHVM_TYPE_POP_R32 equ 0x3
SEHVM_TYPE_JMP_REL_C32 equ 0x4
SEHVM_TYPE_CALL_REL_C32 equ 0x5
SEHVM_REAX equ 0x0
SEHVM_REBX equ 0x1
SEHVM_RECX equ 0x2
SEHVM_REDX equ 0x3
SEHVM_RESI equ 0x4
SEHVM_REDI equ 0x5
SEHVM_REBP equ 0x6
SEHVM_RINVALID equ 0x7
SEHVM_INT3_OPCODE equ 0xCC

macro SEHVM_PUSH_R32 r32val {db SEHVM_INT3_OPCODE, SEHVM_TYPE_PUSH_R32, r32val}

macro SEHVM_PUSH_C32 c32val {
  db SEHVM_INT3_OPCODE, SEHVM_TYPE_PUSH_C32
  dd c32val
}

macro SEHVM_POP_R32 r32val { db SEHVM_INT3_OPCODE, SEHVM_TYPE_POP_R32, r32val }

macro SEHVM_NOP { db SEHVM_INT3_OPCODE, SEHVM_TYPE_POP_R32, SEHVM_INVALID_OPCODE }

macro SEHVM_RET_C8 x {db SEHVM_INT3_OPCODE, SEHVM_TYPE_RET_C8, x}

macro SEHVM_JMP_REL_C32 x {
    local ..m
    ..m:
    db SEHVM_INT3_OPCODE, SEHVM_TYPE_JMP_REL_C32
    dd x-..m
}

macro SEHVM_CALL_REL_C32 x {
    local ..m
    ..m:
    db SEHVM_INT3_OPCODE, SEHVM_TYPE_CALL_REL_C32
    dd x-..m
}

