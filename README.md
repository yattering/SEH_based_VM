# SEH based VM Engine by Yattering #

This is simple VM engine for Win32 x86 code virtualize, what can replace some machine command by own _equal_ VM commands, so protected code can't run without external VM Engine. This project is only proof of concept, it be written for educational purposes.

## Instruction Set ##

- `RET_C8` Return from a subroutine, using the top of the stack as the new instruction pointer and add to stack pointer 8-bit argument `C8`.  

- `PUSH_C32` Push the value of 32-bit constant argument `C32` onto the stack and decrement the stack pointer.

- `PUSH_R32` Push the value of 32-bit general purpose register argument `R32` to top of stack, in case of `INVALID` push value is 32-bit constant `0xDEADBEEF`. Argument `R32` is any of general purposes x86 32-bit register (except `ESP`) and `INVALID`, so valid arguments is <`EAX` `EBX` `ECX` `EDX` `ESI` `EDI` `EBP` `INVALID`>.

- `POP_R32` Pop the value on top of the stack into 32-bit general purpose register argument `R32`, in case of `INVALID` it nothing to do. Argument `R32` is any of general purposes x86 32-bit register (except `ESP`) and `INVALID`, so valid arguments is <`EAX` `EBX` `ECX` `EDX` `ESI` `EDI` `EBP` `INVALID`>.

- `JMP_REL_C32` Jump to a subroutine at  32-bit constant argument `C32` with relative address of function.

- `CALL_REL_C32` Jump to a subroutine at  32-bit constant argument `C32` with relative address of function and put the current instruction pointer onto the stack.

- `NOP` This instruction nothing to do and alias to `SEHVM_PUSH_R32 RINVALID`.

## Files ##

- `compile.cmd`
- `debug_log.h` C header file for debug listing generation.
- `exception_handler.c` Main project file with SEH exception handler function.
- `EXCEPTION_HANDLER.OBJ` Compiled `exception_handler.c` into MS COFF file without any additional definition. This is linkable core of VM.
- `readme.md` This readme file.
- `sehvm.h` C header file for embedded VM Engine in your project.
- `sehvm.inc` FASM include file for embedded VM Engine in your project.
- `demo_fasm.asm` Exmaple of protect code in FASM (source code).
- `demo_fasm.exe` Exmaple of protect code in FASM (binary).
- `SELF_TESTING.exe` Compiled `exception_handler.c` into WIN32 PE x86 executable with `SELF_TEST` definition. This example contain testing of every command from instruction set.
- `SELF_TESTING.OBJ` Compiled `exception_handler.c` into MS COFF for produce WIN32 PE x86 executable with `SELF_TEST` definition. This example contain testing of every command from instruction set.
- `logo.gif`

## Contacts ##

If you have any question, you can find me by contacts below:
- e-mail: yattering (at) sigaint (d0t) org
- jabber: yattering (at) xmpp (d0t) jp

_to be continued..._

![Coded by Yattering](https://gitlab.com/yattering/SEH_based_VM/raw/master/logo.gif)
